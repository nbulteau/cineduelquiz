 'use strict';

angular.module('cineduelquizApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-cineduelquizApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-cineduelquizApp-params')});
                }
                return response;
            },
        };
    });