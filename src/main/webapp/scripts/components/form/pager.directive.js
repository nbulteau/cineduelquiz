/* globals $ */
'use strict';

angular.module('cineduelquizApp')
    .directive('cineduelquizAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
