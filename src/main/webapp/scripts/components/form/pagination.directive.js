/* globals $ */
'use strict';

angular.module('cineduelquizApp')
    .directive('cineduelquizAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
