'use strict';

angular.module('cineduelquizApp').factory('Quiz', function($resource, DateUtils) {
    return $resource('api/quizzes/:id', {}, {
        'query' : {
            method : 'GET',
            isArray : true
        },
        'get' : {
            method : 'GET',
            transformResponse : function(data) {
                data = angular.fromJson(data);
                return data;
            }
        }
    });
});
