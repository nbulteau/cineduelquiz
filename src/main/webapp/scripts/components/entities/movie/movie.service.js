'use strict';

angular.module('cineduelquizApp').factory('Movie', function($resource, DateUtils) {
    return $resource('api/movies/:id', {}, {
        'query' : {
            method : 'GET',
            isArray : true
        },
        'get' : {
            method : 'GET',
            transformResponse : function(data) {
                data = angular.fromJson(data);
                return data;
            }
        }
    });
});
