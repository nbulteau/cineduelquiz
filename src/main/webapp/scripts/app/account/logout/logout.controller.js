'use strict';

angular.module('cineduelquizApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
