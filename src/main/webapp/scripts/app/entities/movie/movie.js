'use strict';

angular.module('cineduelquizApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('movie', {
                parent: 'entity',
                url: '/movies',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Movies'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/movie/movies.html',
                        controller: 'MovieController'
                    }
                },
                resolve: {
                }
            })
            .state('movie.detail', {
                parent: 'entity',
                url: '/movie/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Movie'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/movie/movie-detail.html',
                        controller: 'MovieDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Movie', function($stateParams, Movie) {
                        return Movie.get({id : $stateParams.id});
                    }]
                }
            });
    });
