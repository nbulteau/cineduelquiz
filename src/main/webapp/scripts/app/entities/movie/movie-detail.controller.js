'use strict';

angular.module('cineduelquizApp')
    .controller('MovieDetailController', function ($scope, $rootScope, $stateParams, entity, Movie) {
        $scope.movie = entity;
        $scope.load = function (id) {
            Movie.get({id: id}, function(result) {
                $scope.movie = result;
            });
        };
        $rootScope.$on('cineduelquizApp:movieUpdate', function(event, result) {
            $scope.movie = result;
        });
        
        
    });
