'use strict';

angular.module('cineduelquizApp')
    .controller('MovieController', function ($scope, Movie, ParseLinks) {
        $scope.movies = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Movie.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.movies = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.movie = {title: null, id: null};
        };
    });
