'use strict';

angular.module('cineduelquizApp').controller('QuizController', function($scope, Quiz, ParseLinks) {
    $scope.quizzes = [];
    $scope.page = 0;
    $scope.loadAll = function() {
        Quiz.query({
            page : $scope.page,
            size : 20
        }, function(result, headers) {
            $scope.links = ParseLinks.parse(headers('link'));
            $scope.quizzes = result;
        });
    };
    $scope.loadPage = function(page) {
        $scope.page = page;
        $scope.loadAll();
    };

    $scope.loadAll();

    $scope.refresh = function() {
        $scope.loadAll();
        $scope.clear();
    };

    $scope.clear = function() {
        $scope.quiz = {
            title : null,
            id : null
        };
    };

    // add a quiz

    $scope.types = [ 'WitchActors', 'ACastingWitchMovie', 'FromWitchAnimationComeThisHero' ];

    $scope.nbQuestions = [ '06', '07', '08', '09', '10' ];

    $scope.difficulties = [ 'Noob', 'Expert', 'Nightmare' ];

    $scope.add = function() {
        $scope.quiz = {
            'type' : 'WitchActors',
            'nbQuestions' : '10',
            'difficulty' : 'Expert'
        };

        $('#quizModal').modal('show');
    }

    $scope.cancelCreateQuiz = function() {
        $('#quizModal').modal('hide');
    }

    $scope.submitQuiz = function() {
        Quiz.save($scope.quiz, function() {
            $scope.refresh();
        });
        $('#quizModal').modal('hide');
    }

});
