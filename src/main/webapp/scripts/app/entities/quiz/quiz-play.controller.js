'use strict';

angular.module('cineduelquizApp').controller('QuizPlayController',
        function($scope, $rootScope, $stateParams, $timeout, entity, Quiz) {
            $scope.quiz = entity;
            $scope.finished = false;

            // move to next card
            function moveToNextCard() {
            //$scope.moveToNextCard = function() {
                $scope.currentStats = computeStatsForSession();
                if ($scope.cards.length > 0) {
                    $scope.curIndex = Math.floor((Math.random() * $scope.cards.length));
                    $scope.currentCard = $scope.cards[$scope.curIndex];
                } else {
                    $scope.started = false;
                    $scope.finished = true;
                }
            };

            $scope.response = function(idAnswser) {
                markCurrentCard(idAnswser);

                // Blink correct answer
                var button = document.getElementById("answer0");
                button.classList.add('blink'); 
                // Set all wrong answers in red
                button = document.getElementById("answer1");
                button.style.background="red";
                button = document.getElementById("answer2");
                button.style.background="red";
                button = document.getElementById("answer3");
                button.style.background="red";                
                
                $timeout(moveToNextCard, 2000);
            };

            function markCurrentCard(idAnswser) {
//            $scope.markCurrentCard = function(idAnswser) {
                angular.forEach($scope.currentCard.possibleAnswsers, function(answer) {
                    if (answer.id === idAnswser) {
                        // Save the answer
                        $scope.currentCard.answer = answer;
                    }
                });
                // Removing card from session if necessary
                $scope.cards.splice($scope.curIndex, 1);
            };

            function computeStatsForSession() {
//            $scope.computeStatsForSession = function() {
                // Json to be returned with count of cards in each category
                var stats = {
                    success : 0,
                    fail : 0,
                    notYetViewed : 0
                }

                // Iterates on the all the cards selected for session
                // Note : some cards may have not yet been viewed
                angular.forEach($scope.selectedCardsForSession, function(card) {
                    var isNotYetViewed = (card.answer === undefined);
                    var isFail, isSuccess;

                    if (isNotYetViewed) {
                        isFail = isSuccess = false;
                    } else {
                        isFail = isSuccess = true;
                        if (card.answer.id === card.question.correctAnswer.id) {
                            isFail = false;
                        }
                        isSuccess = !isFail;
                    }

                    // Allocate the points
                    if (isSuccess) {
                        stats.success++;
                    } else if (isFail) {
                        stats.fail++;
                    } else if (isNotYetViewed) {
                        stats.notYetViewed++
                    }
                    stats.score = Math.round(stats.success * 100 / $scope.max);
                });

                return stats;
            };

            function shuffle(array) {
                var counter = array.length, temp, index;

                // While there are elements in the array
                while (counter > 0) {
                    // Pick a random index
                    index = Math.floor(Math.random() * counter);

                    // Decrease counter by 1
                    counter--;

                    // And swap the last element with it
                    temp = array[counter];
                    array[counter] = array[index];
                    array[index] = temp;
                }

                return array;
            }

            $scope.start = function() {
                $scope.started = true;

                $scope.cards = [];
                $scope.selectedCardsForSession = [];
                angular.forEach($scope.quiz.multipleChoiceQuestions, function(question) {
                    var card = {
                        'question' : question,
                        'possibleAnswsers' : [],
                        'answer' : undefined
                    }

                    // Add answers
                    card.possibleAnswsers.push(question.correctAnswer);
                    card.possibleAnswsers.push.apply(card.possibleAnswsers, question.wrongAnswsers);

                    // shuffle answers in array
                    card.possibleAnswsers = shuffle(card.possibleAnswsers);

                    $scope.cards.push(card);
                    $scope.selectedCardsForSession.push(card);
                });
                $scope.max = $scope.selectedCardsForSession.length;
                moveToNextCard();
                computeStatsForSession();
            }
        });
