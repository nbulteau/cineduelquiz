'use strict';

angular.module('cineduelquizApp').config(function($stateProvider) {
    $stateProvider.state('quiz', {
        parent : 'entity',
        url : '/quizzes',
        data : {
            authorities : [ 'ROLE_USER' ],
            pageTitle : 'Quizzes'
        },
        views : {
            'content@' : {
                templateUrl : 'scripts/app/entities/quiz/quizzes.html',
                controller : 'QuizController'
            }
        },
        resolve : {}
    }).state('quiz.detail', {
        parent : 'entity',
        url : '/quiz/detail/{id}',
        data : {
            authorities : [ 'ROLE_ADMIN' ],
            pageTitle : 'Quiz'
        },
        views : {
            'content@' : {
                templateUrl : 'scripts/app/entities/quiz/quiz-detail.html',
                controller : 'QuizDetailController'
            }
        },
        resolve : {
            entity : [ '$stateParams', 'Quiz', function($stateParams, Quiz) {
                return Quiz.get({
                    id : $stateParams.id
                });
            } ]
        }
    }).state('quiz.play', {
        parent : 'entity',
        url : '/quiz/play/{id}',
        data : {
            authorities : [ 'ROLE_USER' ],
            pageTitle : 'Play quiz'
        },
        views : {
            'content@' : {
                templateUrl : 'scripts/app/entities/quiz/quiz-play.html',
                controller : 'QuizPlayController'
            }
        },
        resolve : {
            entity : [ '$stateParams', 'Quiz', function($stateParams, Quiz) {
                return Quiz.get({
                    id : $stateParams.id
                });
            } ]
        }
    });
});
