package fr.nbu.cdq.web.rest.dto;

import lombok.Data;
import fr.nbu.cdq.domain.quiz.Difficulty;
import fr.nbu.cdq.domain.quiz.Theme;
import fr.nbu.cdq.domain.quiz.Type;

@Data
public class CreateQuizRequestDTO {

    private String name;

    private int nbQuestions;

    private Type type;

    private Difficulty difficulty;

    private Theme theme;
}
