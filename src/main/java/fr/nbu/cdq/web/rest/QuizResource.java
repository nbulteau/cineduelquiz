package fr.nbu.cdq.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.nbu.cdq.domain.quiz.Quiz;
import fr.nbu.cdq.repository.QuizRepository;
import fr.nbu.cdq.security.AuthoritiesConstants;
import fr.nbu.cdq.service.QuizService;
import fr.nbu.cdq.web.rest.dto.CreateQuizRequestDTO;
import fr.nbu.cdq.web.rest.util.HeaderUtil;
import fr.nbu.cdq.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Quiz.
 */
@RestController
@RequestMapping("/api")
public class QuizResource {

    private final Logger log = LoggerFactory.getLogger(QuizResource.class);

    @Inject
    private QuizRepository quizRepository;

    @Inject
    private QuizService quizService;

    /**
     * POST /quizzes -> Create a new quiz.
     */
    @RequestMapping(value = "/quizzes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<?> createQuiz(@RequestBody CreateQuizRequestDTO createQuizRequest) throws URISyntaxException {
        log.debug("REST request to generate a Quiz {}", createQuizRequest);

        Quiz result = quizService.generateQuiz(createQuizRequest.getName(), createQuizRequest.getType(), createQuizRequest.getTheme(),
                createQuizRequest.getDifficulty(), createQuizRequest.getNbQuestions());

        if (result != null) {
            return ResponseEntity.created(new URI("/api/quizzes/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert("quiz", "" + result.getId()))
                    .body(result);
        } else {
            return new ResponseEntity<>("Unable to generate the quiz with parameters", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * GET /quiz -> get all the quiz.
     */
    @RequestMapping(value = "/quizzes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Quiz>> getAllQuizzes(final Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get all quizzes page : {}", pageable);

        Page<Quiz> page = quizRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/quizs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET /quizzes/:id -> get the "id" quiz.
     */
    @RequestMapping(value = "/quizzes/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Quiz> getQuiz(@PathVariable String id) {
        log.debug("REST request to get Quiz : {}", id);
        return Optional.ofNullable(quizRepository.findOne(id)).map(quiz -> new ResponseEntity<>(quiz, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
