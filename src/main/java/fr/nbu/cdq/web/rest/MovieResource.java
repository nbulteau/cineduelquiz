package fr.nbu.cdq.web.rest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.repository.MovieRepository;
import fr.nbu.cdq.service.allocine.AlloCineService;
import fr.nbu.cdq.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Movie.
 */
@RestController
@RequestMapping("/api")
public class MovieResource {

    private final Logger log = LoggerFactory.getLogger(MovieResource.class);

    @Inject
    private MovieRepository movieRepository;

    @Inject
    AlloCineService alloCineService;

    /**
     * GET /movies -> get all the movies.
     */
    @RequestMapping(value = "/movies", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Movie>> getAllMovies(final Pageable pageable) throws URISyntaxException {
        Pageable moviePageable = pageable;
        if (pageable.getSort() == null) {
            Sort sort = new Sort(Direction.ASC, "title");
            moviePageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
        }

        Page<Movie> page = movieRepository.findAll(moviePageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/movies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET /movies/:id -> get the "id" movie.
     */
    @RequestMapping(value = "/movies/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Movie> getMovie(@PathVariable long id) {
        log.debug("REST request to get Movie : {}", id);
        return Optional.ofNullable(movieRepository.findOne(id)).map(movie -> new ResponseEntity<>(movie, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET /movies/title/:title -> get all the movies by title.
     */
    @RequestMapping(value = "/movies/title/{title}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Movie>> getMovie(@PathVariable String title) {
        log.debug("REST request to get Movie : {}", title);
        return Optional.ofNullable(movieRepository.findByTitle(title)).map(movies -> new ResponseEntity<>(movies, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * PUT /movies/updatemoviesdb -> update movies database from file repository.
     * 
     * @throws IOException
     */
    @RequestMapping(value = "/movies/updatemoviesdb", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> updateMoviesDB() throws IOException {
        log.debug("REST request to update DB");

        alloCineService.saveAllMoviesFromFileRepository();

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
