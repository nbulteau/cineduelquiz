package fr.nbu.cdq.web.rest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import fr.nbu.cdq.domain.allocine.movie.person.Person;
import fr.nbu.cdq.repository.PersonRepository;
import fr.nbu.cdq.service.allocine.AlloCineService;
import fr.nbu.cdq.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Person.
 */
@RestController
@RequestMapping("/api")
public class PersonResource {

    private final Logger log = LoggerFactory.getLogger(PersonResource.class);

    @Inject
    private PersonRepository personRepository;

    @Inject
    AlloCineService alloCineService;

    /**
     * GET /persons -> get all the persons.
     */
    @RequestMapping(value = "/persons", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Person>> getAllPersons(final Pageable pageable) throws URISyntaxException {
        Pageable personPageable = pageable;
        if (pageable.getSort() == null) {
            Sort sort = new Sort(Direction.ASC, "title");
            personPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
        }

        Page<Person> page = personRepository.findAll(personPageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/persons");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET /persons/:id -> get the "id" person.
     */
    @RequestMapping(value = "/persons/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Person> getPerson(@PathVariable long id) {
        log.debug("REST request to get Person : {}", id);
        return Optional.ofNullable(personRepository.findOne(id)).map(person -> new ResponseEntity<>(person, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * PUT /persons/updatepersonsdb -> update persons database from file repository.
     * 
     * @throws IOException
     */
    @RequestMapping(value = "/persons/updatepersonsdb", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> updatePersonsDB() throws IOException {
        log.debug("REST request to update DB");

        alloCineService.saveAllPersonsFromFileRepository();

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
