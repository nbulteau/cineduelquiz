/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package fr.nbu.cdq.web.rest.dto;
