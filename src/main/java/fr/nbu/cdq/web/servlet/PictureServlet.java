package fr.nbu.cdq.web.servlet;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.nbu.cdq.repository.file.FilePictureRepository;

public class PictureServlet extends HttpServlet {

    private static final long serialVersionUID = -1L;

    private static final double HEIGHT = 160;

    private final FilePictureRepository posterRepository;

    private final FilePictureRepository photoRepository;

    public PictureServlet(FilePictureRepository filePosterRepository, FilePictureRepository filePhotoRepository) {
        this.posterRepository = filePosterRepository;
        this.photoRepository = filePhotoRepository;
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("image/jpg");

        String id = request.getParameter("id");
        String type = request.getParameter("type");
        if (id != null && type != null) {
            byte[] picture = null;
            if (type.equals("poster")) {
                picture = posterRepository.getPicture(Long.parseLong(id));

            } else if (type.equals("photo")) {
                picture = photoRepository.getPicture(Long.parseLong(id));
            }

            if (picture != null) {
                String scale = request.getParameter("scale");
                if (scale != null) {
                    response.getOutputStream().write(scale(picture));
                } else {
                    response.getOutputStream().write(picture);
                }
            } else {

            }
        }
    }

    public byte[] scale(byte[] poster) throws IOException {
        byte[] imageInByte = null;

        if (poster != null) {
            try (InputStream in = new ByteArrayInputStream(poster)) {
                BufferedImage bufferedImage = ImageIO.read(in);
                double scaleValue = HEIGHT / bufferedImage.getHeight();

                AffineTransform tx = new AffineTransform();
                tx.scale(scaleValue, scaleValue);
                AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
                BufferedImage biNew = new BufferedImage((int) (bufferedImage.getWidth() * scaleValue), (int) (bufferedImage.getHeight() * scaleValue),
                        bufferedImage.getType());

                try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                    ImageIO.write(op.filter(bufferedImage, biNew), "jpg", baos);
                    baos.flush();
                    imageInByte = baos.toByteArray();
                }
            }
        }
        return imageInByte;
    }

}
