package fr.nbu.cdq.service.quiz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fr.nbu.cdq.domain.allocine.movie.CastMember;
import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.allocine.movie.Person;
import fr.nbu.cdq.domain.quiz.Answer;
import fr.nbu.cdq.domain.quiz.MultipleChoiceQuestion;
import fr.nbu.cdq.domain.quiz.Quiz;
import fr.nbu.cdq.domain.quiz.Theme;
import fr.nbu.cdq.domain.quiz.Type;
import fr.nbu.cdq.repository.PersonRepository;
import fr.nbu.cdq.service.allocine.AlloCineService;

@Service
public class WitchActorsQuizGenerator extends QuizGenerator {

    private static final String QUESTION_LABEL = "Quel acteur joue le rôle de \"%s\" dans \"%s\" ?";

    @Inject
    private PersonRepository personRepository;

    @Inject
    private AlloCineService alloCineService;

    /**
     * @param fanCount
     * @return
     */
    @Override
    public List<Movie> getMostRelevantMovies(Theme theme, int fanCount) {

        List<Movie> movies = movieRepository.findMostRelevantMovies(fanCount);

        // Filter movies with Theme
        // TODO

        return movies;
    }

    @Override
    public Quiz generateQuizFromMovies(Type type, int nbQuestions, List<Movie> movies) {
        Set<Long> alreadyUsedMovieIds = new HashSet<>();

        final Quiz quiz = new Quiz(type);
        List<MultipleChoiceQuestion> questions = new ArrayList<>();
        quiz.setMultipleChoiceQuestions(questions);

        for (int index = 0; index < nbQuestions; index++) {
            Set<Person> alreadyUsedPerson = new HashSet<>();

            // Select a movie
            Movie movie = pickARandomMovie(movies, alreadyUsedMovieIds);
            alreadyUsedMovieIds.add(movie.getCode());

            // Get first Actor
            Optional<CastMember> firstCastMember = getFirstActorCastMember(movie);

            MultipleChoiceQuestion question = new MultipleChoiceQuestion(String.format(QUESTION_LABEL, firstCastMember.get().getRole(), movie.getTitle()));
            questions.add(question);
            // Generate correct answer
            Answer answer = new Answer(0, firstCastMember.get().getPerson().getName());
            question.setCorrectAnswer(answer);
            alreadyUsedPerson.add(firstCastMember.get().getPerson());

            int gender = getGender(firstCastMember.get().getPerson().getCode());
            if (gender == 0) {
                gender = 1;
            }

            // Generate wrong answer
            for (int answserIndex = 1; answserIndex < MultipleChoiceQuestion.NB_ANSWSERS_BY_QUESTION; answserIndex++) {
                // Select a person that is not already used.
                Person person;
                Movie wrongAnswserMovie;
                do {
                    wrongAnswserMovie = pickARandomMovie(movies, alreadyUsedMovieIds);
                    person = getFirstActorCastMember(wrongAnswserMovie).get().getPerson();
                } while (alreadyUsedPerson.contains(person) || getGender(person.getCode()) != gender);
                alreadyUsedMovieIds.add(wrongAnswserMovie.getCode());

                Answer wrongAnswer = new Answer(answserIndex, person.getName());
                question.getWrongAnswsers().add(wrongAnswer);
            }
        }
        return quiz;
    }

    private int getGender(long codePerson) {
        fr.nbu.cdq.domain.allocine.movie.person.Person person = personRepository.findOne(codePerson);
        // if (person == null) {
        // person = alloCineService.retrieveAndSavePerson(codePerson);
        // }

        if (person != null) {
            return person.getGender();
        }
        return 0;
    }

    /**
     * Get the first Actor from the castMember list.
     * 
     * @param movie
     * @return
     */
    private Optional<CastMember> getFirstActorCastMember(Movie movie) {
        Optional<CastMember> firstCastMember = movie.getCastMember().stream().filter(c -> c.getRole() != null).findFirst();
        return firstCastMember;
    }
}
