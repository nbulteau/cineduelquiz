package fr.nbu.cdq.service.quiz;

import java.util.List;
import java.util.OptionalInt;
import java.util.Random;
import java.util.Set;

import javax.inject.Inject;

import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.quiz.Difficulty;
import fr.nbu.cdq.domain.quiz.Quiz;
import fr.nbu.cdq.domain.quiz.Theme;
import fr.nbu.cdq.domain.quiz.Type;
import fr.nbu.cdq.repository.MovieRepository;

public abstract class QuizGenerator {

    @Inject
    protected MovieRepository movieRepository;

    private final Random random = new Random();

    public abstract Quiz generateQuizFromMovies(Type type, int nbQuestions, List<Movie> movies);

    /**
     * Pick randomly a movie from the movie list that is not already used.
     * 
     * @param movies
     * @param alreadyUsedMovieIds
     * @return
     */
    protected Movie pickARandomMovie(List<Movie> movies, Set<Long> alreadyUsedMovieIds) {
        Movie selectedMovie;
        OptionalInt randomNumber;
        do {
            randomNumber = random.ints(0, movies.size()).findFirst();
            selectedMovie = movies.get(randomNumber.getAsInt());
        } while (alreadyUsedMovieIds.contains(selectedMovie.getCode()));

        return selectedMovie;
    }

    /**
     * @param difficulty
     * @return
     */
    protected int difficultyToFanCount(Difficulty difficulty) {
        int fanCount = 0;
        switch (difficulty) {
        case Noob:
            fanCount = 1500;
            break;
        case Expert:
            fanCount = 150;
            break;
        case Nightmare:
            fanCount = 15;
            break;
        default:
            break;
        }
        return fanCount;
    }

    /**
     * @param fanCount
     * @return
     */
    protected abstract List<Movie> getMostRelevantMovies(Theme theme, int fanCount);

    public List<Movie> getMovies(Theme theme, Difficulty difficulty) {
        // Calculate difficulty
        int fanCount = difficultyToFanCount(difficulty);

        List<Movie> movies = getMostRelevantMovies(theme, fanCount);

        while (movies.size() < 10 && fanCount >= 0) {
            fanCount -= 100;
            movies = getMostRelevantMovies(theme, fanCount);
        }

        return movies;

    }

}
