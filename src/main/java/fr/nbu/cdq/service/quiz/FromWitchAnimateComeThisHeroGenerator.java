package fr.nbu.cdq.service.quiz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import fr.nbu.cdq.domain.allocine.movie.CastMember;
import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.quiz.Answer;
import fr.nbu.cdq.domain.quiz.MultipleChoiceQuestion;
import fr.nbu.cdq.domain.quiz.Quiz;
import fr.nbu.cdq.domain.quiz.Theme;
import fr.nbu.cdq.domain.quiz.Type;

@Service
public class FromWitchAnimateComeThisHeroGenerator extends QuizGenerator {

    private static final String QUESTION_LABEL = "Dans quel film d'animation peut-on rencontrer \"%s\" ?";

    /**
     * @param fanCount
     * @return
     */
    @Override
    public List<Movie> getMostRelevantMovies(final Theme theme, final int fanCount) {

        final List<Movie> movies = movieRepository.findMostRelevantAnimationMovies(fanCount);

        // Filter movies with Theme
        // TODO

        return movies;
    }

    @Override
    public Quiz generateQuizFromMovies(final Type type, final int nbQuestions, final List<Movie> movies) {
        final Set<Long> alreadyUsedMovieIds = new HashSet<>();

        final Quiz quiz = new Quiz(type);
        final List<MultipleChoiceQuestion> questions = new ArrayList<>();
        quiz.setMultipleChoiceQuestions(questions);

        for (int index = 0; index < nbQuestions; index++) {

            // Select a movie
            final Movie movie = pickARandomMovie(movies, alreadyUsedMovieIds);
            alreadyUsedMovieIds.add(movie.getCode());

            // Get first Actor
            final Optional<CastMember> firstCastMember = getFirstActorCastMember(movie);

            String role = firstCastMember.get().getRole();

            // Clean role
            role = role.replace("Voix de", "");
            role = role.replace("(voix)", "");
            role = role.replace("voix VO de", "");
            role = role.trim();

            final MultipleChoiceQuestion question = new MultipleChoiceQuestion(String.format(QUESTION_LABEL, role));
            questions.add(question);
            // Generate correct answer
            final Answer answer = new Answer(0, movie.getTitle());
            question.setCorrectAnswer(answer);

            // Generate wrong answer
            for (int answserIndex = 1; answserIndex < MultipleChoiceQuestion.NB_ANSWSERS_BY_QUESTION; answserIndex++) {
                // Select a movie that has not that hero
                boolean movieOk = true;
                Movie wrongAnswserMovie;
                do {
                    wrongAnswserMovie = pickARandomMovie(movies, alreadyUsedMovieIds);
                    movieOk = wrongAnswserMovie.getCastMember().stream().filter(c -> c.getRole() != null)
                            .anyMatch(c -> !c.getRole().equals(firstCastMember.get().getRole()));
                } while (!movieOk);
                // alreadyUsedMovieIds.add(wrongAnswserMovie.getCode());

                final Answer wrongAnswer = new Answer(answserIndex, wrongAnswserMovie.getTitle());
                question.getWrongAnswsers().add(wrongAnswer);
            }
        }
        return quiz;
    }

    /**
     * Get the first Actor from the castMember list.
     * 
     * @param movie
     * @return
     */
    private Optional<CastMember> getFirstActorCastMember(final Movie movie) {
        final Optional<CastMember> firstCastMember = movie.getCastMember().stream().filter(c -> c.getRole() != null)
                .filter(c -> c.getPerson().getName() != null).findFirst();
        return firstCastMember;
    }
}
