package fr.nbu.cdq.service.quiz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import fr.nbu.cdq.domain.allocine.movie.CastMember;
import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.quiz.Answer;
import fr.nbu.cdq.domain.quiz.MultipleChoiceQuestion;
import fr.nbu.cdq.domain.quiz.Quiz;
import fr.nbu.cdq.domain.quiz.Theme;
import fr.nbu.cdq.domain.quiz.Type;

@Service
public class ACastingWitchMovieQuizGenerator extends QuizGenerator {

  private static final String QUESTION_LABEL = "Quel film réunit \"%s\" ?";

  /**
   * @param fanCount
   * @return
   */
  @Override
  public List<Movie> getMostRelevantMovies(final Theme theme, final int fanCount) {

    final List<Movie> movies = movieRepository.findMostRelevantMovies(fanCount);

    // Filter movies with Theme
    // TODO

    return movies;
  }

  @Override
  public Quiz generateQuizFromMovies(final Type type, final int nbQuestions, final List<Movie> movies) {
    final Set<Long> alreadyUsedMovieIds = new HashSet<>();

    final Quiz quiz = new Quiz(type);
    final List<MultipleChoiceQuestion> questions = new ArrayList<>();
    quiz.setMultipleChoiceQuestions(questions);

    for (int index = 0; index < nbQuestions; index++) {
      Movie movie;
      List<CastMember> actors;

      // Get 3 first Actors
      do {
        // Select a movie
        movie = pickARandomMovie(movies, alreadyUsedMovieIds);
        alreadyUsedMovieIds.add(Long.valueOf(movie.getCode()));

        actors = movie.getCastMember().stream().filter(c -> c.getRole() != null).limit(3).collect(Collectors.toList());
      } while (actors.size() > 3);

      final String casting = actors.stream().map(c -> c.getPerson().getName()).collect(Collectors.joining(", "));
      final MultipleChoiceQuestion question = new MultipleChoiceQuestion(String.format(QUESTION_LABEL, casting));
      questions.add(question);

      // Generate correct answer
      final Answer answer = new Answer(0, movie.getTitle());
      question.setCorrectAnswer(answer);

      final List<Movie> wrongAnswserMovieList = buildWrongAnswserMovieList(movies, actors, alreadyUsedMovieIds);

      // Generate wrong answer
      final Set<Long> wrongAnswserAlreadyUsedMovieIds = new HashSet<>();
      wrongAnswserAlreadyUsedMovieIds.add(movie.getCode());

      for (int answserIndex = 1; answserIndex < MultipleChoiceQuestion.NB_ANSWSERS_BY_QUESTION; answserIndex++) {
        final Movie wrongAnswserMovie = pickARandomMovie(wrongAnswserMovieList, wrongAnswserAlreadyUsedMovieIds);
        wrongAnswserAlreadyUsedMovieIds.add(wrongAnswserMovie.getCode());

        final Answer wrongAnswer = new Answer(answserIndex, wrongAnswserMovie.getTitle());
        question.getWrongAnswsers().add(wrongAnswer);
      }

    }
    return quiz;
  }

  /**
   * Return a list of movies with the first actor in but not all the others (second one and third one)
   * 
   * @param movies
   * @param alreadyUsedMovieIds
   * @param alreadyUsedMovieIds
   * @param firstActorName
   * @return
   */
  private List<Movie> buildWrongAnswserMovieList(final List<Movie> movies, final List<CastMember> actors, final Set<Long> alreadyUsedMovieIds) {
    final List<Movie> wrongAnswserMovieSet = new ArrayList<>();

    // Get first Actor
    final long firstActorId = actors.get(0).getPerson().getCode();
    final long secondActorId = actors.get(1).getPerson().getCode();
    final long thirdActorId = actors.get(2).getPerson().getCode();

    //@formatter:off
    for (final Movie movie : movies) {
      final List<Long> actorsIdList = movie.getCastMember().stream()
          .filter(c -> c.getRole() != null)
          .map(c -> c.getPerson().getCode())
          .collect(Collectors.toList());

      if (actorsIdList.contains(firstActorId) && !actorsIdList.contains(secondActorId) && !actorsIdList.contains(thirdActorId)) {
        wrongAnswserMovieSet.add(movie);
      }
    }
    //@formatter:on

    // If wrongAnswserMovieList is too small complete list with a random movie
    while (wrongAnswserMovieSet.size() <= 3) {

      final Movie aMovie = pickARandomMovie(movies, alreadyUsedMovieIds);
      if (!wrongAnswserMovieSet.contains(aMovie)) {
        wrongAnswserMovieSet.add(aMovie);
      }
    }

    return wrongAnswserMovieSet;
  }
}
