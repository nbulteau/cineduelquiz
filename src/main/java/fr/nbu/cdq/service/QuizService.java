package fr.nbu.cdq.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.quiz.Difficulty;
import fr.nbu.cdq.domain.quiz.Quiz;
import fr.nbu.cdq.domain.quiz.Theme;
import fr.nbu.cdq.domain.quiz.Type;
import fr.nbu.cdq.repository.QuizRepository;
import fr.nbu.cdq.service.quiz.ACastingWitchMovieQuizGenerator;
import fr.nbu.cdq.service.quiz.FromWitchAnimateComeThisHeroGenerator;
import fr.nbu.cdq.service.quiz.QuizGenerator;
import fr.nbu.cdq.service.quiz.WitchActorsQuizGenerator;

@Service
public class QuizService {

  private final Map<Type, QuizGenerator> quizGenerators;

  @Inject
  private QuizRepository quizRepository;

  @Inject
  public QuizService(final ACastingWitchMovieQuizGenerator aCastingWitchMovieQuizGenerator, final WitchActorsQuizGenerator witchActorsQuizGenerator,
      final FromWitchAnimateComeThisHeroGenerator fromWitchAnimateComeThisHeroGenerator) {
    super();

    // init quizGenerators list
    final Map<Type, QuizGenerator> aMap = new HashMap<>();
    aMap.put(Type.ACastingWitchMovie, aCastingWitchMovieQuizGenerator);
    aMap.put(Type.WitchActors, witchActorsQuizGenerator);
    aMap.put(Type.FromWitchAnimationComeThisHero, fromWitchAnimateComeThisHeroGenerator);

    quizGenerators = Collections.unmodifiableMap(aMap);
  }

  /**
   * Generate a Quiz from a type.
   * 
   * @param name
   * @param type
   * @param nbQuestions
   * @param difficulty
   * @param theme
   * @return
   */
  public Quiz generateQuiz(final String name, final Type type, final Theme theme, final Difficulty difficulty, final int nbQuestions) {
    Quiz quiz = null;

    if (type != null) {

      // Select quiz generator
      final QuizGenerator quizGenerator = quizGenerators.get(type);

      // Get relevant movies
      final List<Movie> movies = quizGenerator.getMovies(theme, difficulty);

      if (movies.size() > nbQuestions) {
        // Generate quiz
        quiz = quizGenerator.generateQuizFromMovies(type, nbQuestions, movies);

        // Quiz name
        if (name == null) {
          quiz.setName(type.getLabel());
        }
        else {
          quiz.setName(name);
        }
        quiz.setDifficulty(difficulty);
        quiz.setTheme(theme);

        // Save quiz
        quizRepository.save(quiz);
      }
    }

    return quiz;
  }

}
