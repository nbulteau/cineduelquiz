package fr.nbu.cdq.service.allocine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.allocine.movie.MovieResult;
import fr.nbu.cdq.domain.allocine.movie.person.Person;
import fr.nbu.cdq.domain.allocine.movie.person.PersonResult;
import fr.nbu.cdq.repository.MovieRepository;
import fr.nbu.cdq.repository.PersonRepository;
import fr.nbu.cdq.repository.file.FileMovieRepository;
import fr.nbu.cdq.repository.file.FilePersonRepository;
import fr.nbu.cdq.repository.file.FilePictureRepository;

@Service
public class AlloCineService {

  private final Logger log = LoggerFactory.getLogger(AlloCineService.class);

  @Inject
  private FileMovieRepository fileMovieRepository;

  @Inject
  private FilePersonRepository filePersonRepository;

  @Inject
  private FilePictureRepository filePosterRepository;

  @Inject
  private FilePictureRepository filePictureRepository;

  @Inject
  private MovieRepository movieRepository;

  @Inject
  private PersonRepository personRepository;

  /**
   * 
   * @param idPerson
   * @return
   */
  public Person retrieveAndSavePerson(final long idPerson) {
    log.info("Retrieve and save person {}", idPerson);

    final ObjectMapper objectMapper = new ObjectMapper();

    Person person = null;
    try {
      final String json = AlloCineHelper.retrievePersonJson(idPerson);
      // Save json file on disk
      filePersonRepository.save(idPerson, json);

      // Convert the json file to AlloCine structure
      final PersonResult personResult = objectMapper.readValue(json, PersonResult.class);
      person = personResult.getPerson();

      // Get the poster if not present on disk
      if (person != null) {
        if (!filePictureRepository.exists(idPerson) && person.getPicture() != null && person.getPicture().getHref() != null) {
          final byte[] picture = AlloCineHelper.downloadPicture(person.getPicture().getHref());
          filePictureRepository.savePicture(idPerson, picture);
        }
      }

      log.info("retrieveMovie id : {} => {} ", person.getCode(), person.getName());
    } catch (RetrieveException | IOException e) {
      log.error("Retrieve movie problem with person id : {} : {}", idPerson, e.getMessage());
    }

    return person;
  }

  /**
   * 
   * @param idMovie
   * @return
   */
  public Movie retrieveAndSaveMovie(final long idMovie) {
    log.info("Retrieve and save movie {}", idMovie);

    final ObjectMapper objectMapper = new ObjectMapper();

    Movie movie = null;
    try {
      final String json = AlloCineHelper.retrieveMovieJson(idMovie);
      // Save json file on disk
      fileMovieRepository.save(idMovie, json);

      // Convert the json file to AlloCine structure
      final MovieResult movieResult = objectMapper.readValue(json, MovieResult.class);
      movie = movieResult.getMovie();

      // Get the poster if not present on disk
      if (movie != null) {
        if (!filePosterRepository.exists(idMovie) && movie.getPoster() != null && movie.getPoster().getHref() != null) {
          final byte[] poster = AlloCineHelper.downloadPicture(movie.getPoster().getHref());
          filePosterRepository.savePicture(idMovie, poster);
        }

        // Get persons the 3 first castMembers
        movie.getCastMember().stream().limit(3)
            .forEach(c -> {
              retrieveAndSavePerson(c.getPerson().getCode());
              try {
                Thread.sleep(2000);
              } catch (final Exception e) {
                // Nothing to do
              }
            });

        log.info("retrieveMovie id : {} => {} ", movie.getCode(), movie.getTitle());
      }

    } catch (RetrieveException | IOException e) {
      log.error("Retrieve movie problem with movie id : {} ", idMovie);
    }

    return movie;
  }

  /**
   * 
   * @return
   */
  public List<Movie> retrieveMovieList() {
    log.info("Retrieve movielist");

    final List<Movie> movies = new ArrayList<>();
    try {
      final List<Movie> alloCineMovies = AlloCineHelper.retrieveMovielist();
      log.info(" => retrieving {} fims ", alloCineMovies.size());
      Movie movie = null;
      for (final Movie alloCineMovie : alloCineMovies) {
        movie = retrieveAndSaveMovie(alloCineMovie.getCode());
        if (movie != null) {
          movies.add(movie);
        }

        // Tempo for AlloCine
        try {
          Thread.sleep(2000);
        } catch (final InterruptedException e) {
          // Empty block
        }
      }
    } catch (final RetrieveException e) {
      log.error("Retrieve movielist problem : ");
      log.error(e.getMessage());
    }

    log.info("Retrieve movielist done");

    return movies;
  }

  /**
   * 
   * @return
   * @throws IOException
   */
  public void saveAllFromFileRepository() throws IOException {
    // Measuring elapsed time using Spring StopWatch
    final StopWatch watch = new StopWatch();
    watch.start("save All movies From File Repository");
    saveAllMoviesFromFileRepository();
    watch.stop();

    watch.start("save All persons From File Repository");
    saveAllPersonsFromFileRepository();
    watch.stop();

    log.debug("Total execution time to create movies in MongoDB : {}", watch);
  }

  /**
   * 
   * @return
   * @throws IOException
   */
  public int saveAllMoviesFromFileRepository() throws IOException {
    final LongAdder movieCount = new LongAdder();
    final LongAdder rejectedMovieCount = new LongAdder();

    final Stream<Movie> movies = fileMovieRepository.getAllMovies();
    movies.forEach(m -> {
      // At least a synopsys
        if (m.getSynopsis() != null) {
          movieRepository.save(m);
          movieCount.increment();
        }
        else {
          rejectedMovieCount.increment();
        }
      });

    log.debug("Nb movies in MongoDB : {}", movieCount.intValue());
    log.debug("Nb movies rejected : {}", rejectedMovieCount.intValue());

    return movieCount.intValue();
  }

  /**
   * 
   * @return
   * @throws IOException
   */
  public int saveAllPersonsFromFileRepository() throws IOException {
    final LongAdder personCount = new LongAdder();

    final Stream<Person> persons = filePersonRepository.getAllPersons();
    persons.forEach(p -> {
      personRepository.save(p);
      personCount.increment();
    });

    log.debug("Nb persons in MongoDB : {}", personCount.intValue());

    return personCount.intValue();
  }
}
