package fr.nbu.cdq.service.allocine;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.allocine.movie.MovieListResult;
import fr.nbu.cdq.domain.allocine.movie.MovieResult;

public class AlloCineHelper {

  private static final Logger LOGGER = LoggerFactory.getLogger(AlloCineHelper.class);

  private static final String ALLOCINE_API_URL = "http://api.allocine.fr/rest/v3";

  private static final String ALLOCINE_PARTNER_KEY = "100043982026";

  private static final String ALLOCINE_SECRET_KEY = "29d185d98c984a359e6e6f26a0474269";

  private static final String USER_AGENT = "Dalvik/1.6.0 (Linux; U; Android 4.2.2; Nexus 4 Build/JDQ39E)";

  private static final String JSON_EXTENSION = ".json";

  /**
   * Method to deserialize JSON content from given file into Movie structure.
   * 
   * @param file
   * @return Movie
   */
  public static Movie read(final File file) {
    if (file == null) {
      throw new RuntimeException("file can't be null");
    }
    LOGGER.debug("Entering read({})", file.getName());

    final ObjectMapper objectMapper = new ObjectMapper();

    Movie movie = null;

    try {
      final MovieResult movieResult = objectMapper.readValue(file, MovieResult.class);
      movie = movieResult.getMovie();
    } catch (final JsonParseException e) {
      throw new RuntimeException("Underlying input contains invalid content supports for file : " + file.getName(), e);
    } catch (final JsonMappingException e) {
      throw new RuntimeException("Input JSON structure does not match structure expected for Movie type for file : " + file.getName(), e);
    } catch (final IOException e) {
      throw new RuntimeException("I/O problem (unexpected end-of-input, network error) occurs for file : " + file.getName(), e);
    }

    return movie;
  }

  /**
   * Method to deserialize all "*.json" content files with given EXTENSION from given directory path into
   * Movie structure.
   * 
   * @param filesPath
   * @return List of Movie
   * @throws IOException
   */
  public static List<Movie> read(final Path filesPath) throws IOException {
    if (filesPath == null) {
      throw new RuntimeException("filesPath can't be null");
    }
    LOGGER.debug("Entering read({})", filesPath);

    //@formatter:off
    final List<Movie> alloCineMovieList = Files.list(filesPath)
        .filter(path -> path.toString().endsWith(JSON_EXTENSION))
        .map(p -> read(p.toFile()))
        .collect(Collectors.toList());
    //@formatter:on

    return alloCineMovieList;
  }

  /**
   * Retrieve a movie from allocine with the alloCineMovieId.
   * 
   * @param alloCineMovieId
   * @return
   * @throws RetrieveException
   */
  public static Movie retrieveMovie(final long alloCineMovieId) throws RetrieveException {
    LOGGER.debug("Entering retrieveMovie({})", alloCineMovieId);
    final ObjectMapper objectMapper = new ObjectMapper();

    Movie movie = null;
    try {
      final String content = retrieveMovieJson(alloCineMovieId);
      final MovieResult movieResult = objectMapper.readValue(content, MovieResult.class);
      movie = movieResult.getMovie();
    } catch (final JsonParseException e) {
      throw new RetrieveException("Underlying input contains invalid content supports", e);
    } catch (final JsonMappingException e) {
      throw new RetrieveException("Input JSON structure does not match structure expected for Movie type", e);
    } catch (final IOException e) {
      throw new RetrieveException("I/O problem (unexpected end-of-input, network error) occurs", e);
    }

    return movie;
  }

  /**
   * Retrieve a person from allocine with the alloCineMovieId.
   * 
   * @param alloCineMovieId
   * @return
   * @throws RetrieveException
   */
  public static Movie retrievePerson(final long alloCinePersonId) throws RetrieveException {
    LOGGER.debug("Entering retrievePerson({})", alloCinePersonId);
    final ObjectMapper objectMapper = new ObjectMapper();

    Movie movie = null;
    try {
      final String content = retrievePersonJson(alloCinePersonId);
      final MovieResult movieResult = objectMapper.readValue(content, MovieResult.class);
      movie = movieResult.getMovie();
    } catch (final JsonParseException e) {
      throw new RetrieveException("Underlying input contains invalid content supports", e);
    } catch (final JsonMappingException e) {
      throw new RetrieveException("Input JSON structure does not match structure expected for Movie type", e);
    } catch (final IOException e) {
      throw new RetrieveException("I/O problem (unexpected end-of-input, network error) occurs", e);
    }

    return movie;
  }

  /**
   * Retrieve a movie (Json) from allocine with the alloCineMovieId.
   * 
   * @param alloCineMovieId
   * @return
   * @throws RetrieveException
   */
  public static String retrieveMovieJson(final long alloCineMovieId) throws RetrieveException {
    LOGGER.debug("Entering retrieveJson({})", alloCineMovieId);

    String content = null;
    try {
      final String query = buildRetreiveMovieQuery(alloCineMovieId);
      LOGGER.debug("Query : {}", query);

      final HttpResponse response = executeQuery(query, USER_AGENT);

      // Get-Capture Complete JSON body response
      content = IOUtils.toString(new InputStreamReader(response.getEntity().getContent()));
    } catch (final IOException e) {
      throw new RetrieveException("I/O problem (unexpected end-of-input, network error) occurs", e);
    }

    return content;
  }

  /**
   * Retrieve a person (Json) from allocine with the alloCinePersoneId.
   * 
   * @param alloCineMovieId
   * @return
   * @throws RetrieveException
   */
  public static String retrievePersonJson(final long alloCinePersonId) throws RetrieveException {
    LOGGER.debug("Entering retrieveJson({})", alloCinePersonId);

    String content = null;
    try {
      final String query = buildRetreivePersonQuery(alloCinePersonId);
      LOGGER.debug("Query : {}", query);

      final HttpResponse response = executeQuery(query, USER_AGENT);

      // Get-Capture Complete JSON body response
      content = IOUtils.toString(new InputStreamReader(response.getEntity().getContent()));

      System.out.println(content);
    } catch (final IOException e) {
      throw new RetrieveException("I/O problem (unexpected end-of-input, network error) occurs", e);
    }

    return content;
  }

  /**
   * Retrieve the a new movies list from allocine.
   * 
   * @return
   * @throws RetrieveException
   */
  public static List<Movie> retrieveMovielist() throws RetrieveException {
    LOGGER.debug("Entering retrieveMovielist()");
    final ObjectMapper objectMapper = new ObjectMapper();

    final List<Movie> movies = new ArrayList<>();
    int totalResults = Integer.MAX_VALUE;
    final int count = 10;
    int page = 1;

    while (count * page < totalResults) {
      try {
        final String query = buildRetrieveMovieListQuery(page);
        LOGGER.info("Query : {}", query);

        final HttpResponse response = executeQuery(query, USER_AGENT);

        // Get-Capture Complete JSON body response
        final MovieListResult movieListResult = objectMapper.readValue(new InputStreamReader((response.getEntity().getContent())), MovieListResult.class);
        movies.addAll(movieListResult.getFeed().getMovie());
        totalResults = movieListResult.getFeed().getTotalResults();
        page = movieListResult.getFeed().getPage() + 1;

        Thread.sleep(2000); // Tempo for AlloCine
      } catch (final JsonParseException e) {
        throw new RetrieveException("Underlying input contains invalid content supports", e);
      } catch (final JsonMappingException e) {
        throw new RetrieveException("Input JSON structure does not match structure expected for Movie type", e);
      } catch (final IOException e) {
        throw new RetrieveException("I/O problem (unexpected end-of-input, network error) occurs", e);
      } catch (final InterruptedException e) {
        throw new RetrieveException("Thread interrupted", e);
      }
    }

    return movies;
  }

  private static String buildRetrieveMovieListQuery(final int page) throws UnsupportedEncodingException {
    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYMMdd");
    String query = ALLOCINE_API_URL + "/movielist?";

    String params = "partner=" + ALLOCINE_PARTNER_KEY;
    // params += "&count=" + count;
    params += "&filter=nowshowing&format=json";
    params += "&page=" + page;
    params += "&sed=" + simpleDateFormat.format(new Date());
    final String sig = buildAlloCineSignature(params);
    query += params + sig;
    return query;
  }

  private static String buildRetreiveMovieQuery(final long alloCineMovieId) throws UnsupportedEncodingException {
    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYMMdd");
    String query = ALLOCINE_API_URL + "/movie?";

    String params = "partner=" + ALLOCINE_PARTNER_KEY;
    params += "&code=" + alloCineMovieId;
    params += "&format=json&profile=medium&filter=movie,person&striptags=synopsis%2Csynopsisshort";
    params += "&sed=" + simpleDateFormat.format(new Date());
    final String sig = buildAlloCineSignature(params);
    query += params + sig;
    return query;
  }

  private static String buildRetreivePersonQuery(final long alloCinePersoneId) throws UnsupportedEncodingException {
    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYMMdd");
    String query = ALLOCINE_API_URL + "/person?";

    String params = "partner=" + ALLOCINE_PARTNER_KEY;
    params += "&code=" + alloCinePersoneId;
    params += "&profile=large";
    params += "&sed=" + simpleDateFormat.format(new Date());
    final String sig = buildAlloCineSignature(params);
    query += params + sig;

    return query;
  }

  private static HttpResponse executeQuery(final String query, final String userAgent) throws IOException, ClientProtocolException {
    final URI uri = URI.create(query);
    // create HTTP Client
    final HttpClient httpClient = HttpClientBuilder.create().build();
    // Create new getRequest with below mentioned URL
    final HttpGet getRequest = new HttpGet(uri);
    // Add additional header to getRequest which accepts application/xml data
    getRequest.addHeader("User-Agent", userAgent);
    // Execute your request and catch response
    final HttpResponse response = httpClient.execute(getRequest);

    // Check for HTTP response code: 200 = success
    if (response.getStatusLine().getStatusCode() != 200) {
      throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
    }

    return response;
  }

  private static String buildAlloCineSignature(final String params) throws UnsupportedEncodingException {
    final String sha1params = computeSha1OfString(ALLOCINE_SECRET_KEY + params);
    return "&sig=" + URLEncoder.encode(sha1params, "UTF-8");
  }

  private static String computeSha1OfString(final String arg) {
    try {
      return computeSha1OfByteArray(arg.getBytes(("UTF-8")));
    } catch (final UnsupportedEncodingException ex) {
      throw new UnsupportedOperationException(ex);
    }
  }

  private static String computeSha1OfByteArray(final byte[] arg) {
    try {
      final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
      messageDigest.update(arg);
      final byte[] res = messageDigest.digest();

      return new String(java.util.Base64.getMimeEncoder().encode(res));
    } catch (final NoSuchAlgorithmException ex) {
      throw new UnsupportedOperationException(ex);
    }
  }

  /**
   * Download the picture
   * 
   * @param href
   *            picture URL
   * @return byte array of the download picture
   * @throws IOException
   */
  static byte[] downloadPicture(final String href) throws IOException {
    LOGGER.debug("Entering downloadPicture : {}", href);
    byte[] imageInByte = null;

    if (href != null) {
      final URL url = new URL(href);
      final BufferedImage bufferedImage = ImageIO.read(url);
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
        ImageIO.write(bufferedImage, "jpg", baos);
        baos.flush();
        imageInByte = baos.toByteArray();
      }
    }
    return imageInByte;
  }
}
