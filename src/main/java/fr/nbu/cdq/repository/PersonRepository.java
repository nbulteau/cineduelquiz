package fr.nbu.cdq.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.nbu.cdq.domain.allocine.movie.person.Person;

/**
 * Spring Data MongoDB repository for the Movie entity.
 */
public interface PersonRepository extends MongoRepository<Person, Long> {
  // Empty block
}
