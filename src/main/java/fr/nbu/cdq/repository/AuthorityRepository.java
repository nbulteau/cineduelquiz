package fr.nbu.cdq.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.nbu.cdq.domain.Authority;

/**
 * Spring Data MongoDB repository for the Authority entity.
 */
public interface AuthorityRepository extends MongoRepository<Authority, String> {
  // Empty block
}
