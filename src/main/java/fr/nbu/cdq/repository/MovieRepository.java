package fr.nbu.cdq.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import fr.nbu.cdq.domain.allocine.movie.Movie;

/**
 * Spring Data MongoDB repository for the Movie entity.
 */
public interface MovieRepository extends MongoRepository<Movie, Long> {

    List<Movie> findByTitle(String title);

    /**
     * All movies that are not animation and that have a fan count greater than fanCount param.
     * 
     * @param fanCount
     * @return
     */
    @Query("{'statistics.fanCount' : { '$gt' : ?0 }, 'genre.code' : { $ne: 13026 } })")
    List<Movie> findMostRelevantMovies(int fanCount);

    /**
     * All movies that are animation and that have a fan count greater than fanCount param.
     * 
     * @param fanCount
     * @return
     */
    @Query("{'statistics.fanCount' : { '$gt' : ?0 }, 'genre.code' : { $eq: 13026 } })")
    List<Movie> findMostRelevantAnimationMovies(int fanCount);
}
