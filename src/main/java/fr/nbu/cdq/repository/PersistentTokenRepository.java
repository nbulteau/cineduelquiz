package fr.nbu.cdq.repository;

import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.data.mongodb.repository.MongoRepository;

import fr.nbu.cdq.domain.PersistentToken;
import fr.nbu.cdq.domain.User;

/**
 * Spring Data MongoDB repository for the PersistentToken entity.
 */
public interface PersistentTokenRepository extends MongoRepository<PersistentToken, String> {

    List<PersistentToken> findByUser(User user);

    List<PersistentToken> findByTokenDateBefore(LocalDate localDate);

}
