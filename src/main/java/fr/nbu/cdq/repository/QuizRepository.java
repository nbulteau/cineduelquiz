package fr.nbu.cdq.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.nbu.cdq.domain.quiz.Quiz;

public interface QuizRepository extends MongoRepository<Quiz, String> {
  // Empty block
}
