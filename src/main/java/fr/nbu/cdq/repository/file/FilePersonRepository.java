package fr.nbu.cdq.repository.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nbu.cdq.domain.allocine.movie.person.Person;
import fr.nbu.cdq.domain.allocine.movie.person.PersonResult;
import fr.nbu.cdq.service.UserService;

/**
 * 
 * An FilePersonRepository contains jsons.
 * 
 * allocine
 * --jsons
 * ----0
 * ------1.json
 * ------2.json
 * ----1
 * ------100012.json
 * 
 * * @author Nicolas
 *
 */
@Repository("filePersonRepository")
public class FilePersonRepository extends FileRepository {
    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Inject
    public FilePersonRepository(@Value("${filerepository.filepersons.path}") String repositoryPath, @Value("${filerepository.filepersons.suffix}") String suffix) {
        super(repositoryPath, suffix);
    }

    /**
     * Save a Person structure in a file.
     * 
     * @param id
     * @param person
     */
    public void save(PersonResult person) {

        File file = getFile(person.getPerson().getCode());
        try {
            objectMapper.writeValue(file, person);
        } catch (IOException e) {
            log.error("Was saving " + file.getName(), e);
        }
    }

    /**
     * Find a Person by it's id.
     * 
     * @param id
     * @return
     */
    public Person findOne(long id) {
        return load(getFile(id));
    }

    /**
     * Delete a Person by it's id.
     * 
     * @param id
     * @return
     */
    public boolean delete(long id) {
        return getFile(id).delete();
    }

    /**
     * Load a File and convert the json.
     * 
     * @param file
     * @return
     */
    private Person load(File file) {
        if (file != null && file.exists()) {
            try {
                PersonResult alloPerson = objectMapper.readValue(file, PersonResult.class);
                return alloPerson.getPerson();
            } catch (IOException e) {
                log.info("Was loading " + file.getName(), e);
            }
        }
        return null;
    }

    public Stream<Person> getAllPersons() throws IOException {
        List<Person> emptyPersonList = new ArrayList<>();
        Stream<Person> persons = emptyPersonList.stream();
        List<Path> allFilepath = getAllFilepath();

        for (Path filesPath : allFilepath) {
            //@formatter:off
            persons = Stream.concat(persons,
             Files.list(filesPath)
                .filter(path -> path.toString().endsWith(suffix))
                .map(p -> load(p.toFile())));
            //@formatter:on
        }
        return persons;
    }
}
