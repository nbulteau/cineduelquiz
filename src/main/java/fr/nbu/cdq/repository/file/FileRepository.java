package fr.nbu.cdq.repository.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * A FileRepository directory is sliced in 0, 1, 2, 3, 4, 5 directories containing the AlloCine ids items (photos,
 * jsons, posters)
 * 
 * @author Nicolas
 *
 */
public abstract class FileRepository {

    private static final int NB_SLICES = 5;

    protected String repositoryPath;

    protected final String suffix;

    public FileRepository(String repositoryPath, String suffix) {
        super();
        this.repositoryPath = repositoryPath;
        this.suffix = suffix;
    }

    public void save(long idMovie, String json) throws IOException {
        File file = getFile(idMovie);
        Path path = Paths.get(file.getPath());

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            writer.write(json);
        }
    }

    public long count() throws IOException {
        long nbFiles = 0;
        for (int index = 0; index <= NB_SLICES; index++) {
            Path filesPath = Paths.get(repositoryPath + File.separator + index + File.separator);
            //@formatter:off
            nbFiles += Files.list(filesPath)
              .filter(path -> path.toString().endsWith(suffix))
              .count();
            //@formatter:on
        }

        return nbFiles;
    }

    public boolean exists(long id) {
        return getFile(id).exists();
    }

    protected File getFile(long id) {
        return new File(repositoryPath + File.separator + (id / 100000) + File.separator + id + suffix);
    }

    protected List<Path> getAllFilepath() {
        List<Path> paths = new ArrayList<>();

        for (int index = 0; index <= NB_SLICES; index++) {
            Path filesPath = Paths.get(repositoryPath + File.separator + index + File.separator);
            paths.add(filesPath);
        }
        return paths;
    }

    /**
     * Extract the id from the filename.
     * 
     * @param file
     * @return
     */
    protected Long extractId(File file) {
        String fileName = file.getName();
        int posPoint = fileName.lastIndexOf('.');
        if (0 < posPoint && posPoint <= fileName.length() - 2) {
            return Long.parseLong(fileName.substring(0, posPoint));
        }
        return null;
    }

}