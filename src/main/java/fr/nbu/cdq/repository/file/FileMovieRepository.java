package fr.nbu.cdq.repository.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.allocine.movie.MovieResult;
import fr.nbu.cdq.service.UserService;

/**
 * 
 * An FileMovieRepository contains jsons.
 * 
 * allocine
 * --jsons
 * ----0
 * ------1.json
 * ------2.json
 * ----1
 * ------100012.json
 * 
 * * @author Nicolas
 *
 */
@Repository("fileMovieRepository")
public class FileMovieRepository extends FileRepository {
    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Inject
    public FileMovieRepository(@Value("${filerepository.filemovies.path}") String repositoryPath, @Value("${filerepository.filemovies.suffix}") String suffix) {
        super(repositoryPath, suffix);
    }

    /**
     * Save a Movie structure in a file.
     * 
     * @param id
     * @param movie
     */
    public void save(MovieResult movie) {

        File file = getFile(movie.getMovie().getCode());
        try {
            objectMapper.writeValue(file, movie);
        } catch (IOException e) {
            log.error("Was saving " + file.getName(), e);
        }
    }

    /**
     * Find a Movie by it's id.
     * 
     * @param id
     * @return
     */
    public Movie findOne(long id) {
        return load(getFile(id));
    }

    /**
     * Delete a Movie by it's id.
     * 
     * @param id
     * @return
     */
    public boolean delete(long id) {
        return getFile(id).delete();
    }

    /**
     * Load a File and convert the json.
     * 
     * @param file
     * @return
     */
    private Movie load(File file) {
        if (file != null && file.exists()) {
            try {
                MovieResult alloMovie = objectMapper.readValue(file, MovieResult.class);
                return alloMovie.getMovie();
            } catch (IOException e) {
                log.info("Was loading " + file.getName(), e);
            }
        }
        return null;
    }

    public Stream<Movie> getAllMovies() throws IOException {
        List<Movie> emptyMovieList = new ArrayList<>();
        Stream<Movie> movies = emptyMovieList.stream();
        List<Path> allFilepath = getAllFilepath();

        for (Path filesPath : allFilepath) {
            //@formatter:off
            movies = Stream.concat(movies,
             Files.list(filesPath)
                .filter(path -> path.toString()
                .endsWith(suffix))
                .map(p -> load(p.toFile())));
            //@formatter:on
        }
        return movies;
    }
}
