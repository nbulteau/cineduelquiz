package fr.nbu.cdq.repository.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

public class FilePictureRepository extends FileRepository {

    private final Logger log = LoggerFactory.getLogger(FilePictureRepository.class);

    public FilePictureRepository(@Value("${filerepository.filepictures.path}") String repositoryPath,
            @Value("${filerepository.filepictures.suffix}") String suffix) {
        super(repositoryPath, suffix);
    }

    public byte[] getPicture(long pictureId) {
        byte[] picture = null;
        File file = getFile(pictureId);
        if (!file.exists()) {
            file = new File(repositoryPath + File.separator + "empty_photo.jpg");
        }
        try (FileInputStream fis = new FileInputStream(file)) {
            picture = new byte[fis.available()];
            fis.read(picture);
        } catch (IOException e) {
            log.error("Was loading " + file.getName(), e);
            return null;
        }

        return picture;
    }

    public void savePicture(long pictureId, byte[] picture) {
        File file = getFile(pictureId);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(picture);
        } catch (IOException e) {
            log.error("Was saving " + file.getName(), e);
        }
    }
}
