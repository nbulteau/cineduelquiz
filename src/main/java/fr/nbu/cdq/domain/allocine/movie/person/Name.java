package fr.nbu.cdq.domain.allocine.movie.person;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("name")
public class Name {

    private String given;

    private String family;

}
