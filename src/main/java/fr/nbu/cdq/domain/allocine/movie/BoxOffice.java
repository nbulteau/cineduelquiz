package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("boxOffice")
public class BoxOffice {

    private Code type;

    private Code country;

    private Period period;

    private int week;

    private int admissionCount;

    private int admissionCountTotal;

    private int copyCount;

    private double gross;

    private double grossTotal;

    private Code currency;

}
