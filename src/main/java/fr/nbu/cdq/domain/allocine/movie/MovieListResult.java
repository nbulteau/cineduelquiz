package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

@Data
public class MovieListResult {
	private Feed feed;
}
