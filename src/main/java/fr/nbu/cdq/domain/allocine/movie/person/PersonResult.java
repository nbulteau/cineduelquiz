package fr.nbu.cdq.domain.allocine.movie.person;

import lombok.Data;

@Data
public class PersonResult {
    private Person person;
}
