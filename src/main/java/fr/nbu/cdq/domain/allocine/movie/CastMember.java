package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("castMember")
public class CastMember {
    private Code activity;

    private Person person;

    private String role;

    private Picture picture;

    private int isOriginalVoice;

    private int isLocalVoice;

}
