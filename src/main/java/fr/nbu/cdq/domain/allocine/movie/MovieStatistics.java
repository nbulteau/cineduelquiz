package fr.nbu.cdq.domain.allocine.movie;

import java.util.List;

import lombok.Data;

import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("statistics")
public class MovieStatistics {

  public MovieStatistics() {

  }

  // workaround for allocine bug
  // JSON occasionally comes from the server as "statistics" : ""
  // which is just plain stupid and makes jackson complain.
  public MovieStatistics(final String dummy) {
    super();
  }

  private float pressRating;

  private int pressReviewCount;

  private float userRating;

  private int userReviewCount;

  private int userRatingCount;

  private int commentCount;

  private int photoCount;

  private int videoCount;

  @JsonIgnore
  private List<Rating> alloCineRating;

  private List<Rating> rating;

  @Indexed(name = "fanCountIndex", direction = IndexDirection.DESCENDING)
  private int fanCount;

  private int releaseWeekPosition;

  private int theaterCount;

  private int theaterCountOnRelease;

  private int editorialRatingCount;

  private int wantToSeeCount;

  private int triviaCount;

  private int rankTopMovie;

  private int variationTopMovie;

  private int admissionCount;

  private int newsCount;

  private int awardCount;

  private int nominationCount;
}
