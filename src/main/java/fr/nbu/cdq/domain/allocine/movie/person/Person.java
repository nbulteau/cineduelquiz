package fr.nbu.cdq.domain.allocine.movie.person;

import java.util.Date;
import java.util.List;

import lombok.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import fr.nbu.cdq.domain.allocine.movie.Code;
import fr.nbu.cdq.domain.allocine.movie.Feature;
import fr.nbu.cdq.domain.allocine.movie.Link;
import fr.nbu.cdq.domain.allocine.movie.Media;
import fr.nbu.cdq.domain.allocine.movie.News;
import fr.nbu.cdq.domain.allocine.movie.Picture;

@Data
@JsonTypeName("person")
@Document
public class Person {

    @Id
    private long code;

    private Name name;

    private short gender;

    private List<Code> nationality;

    private List<Code> activity;

    private String activityShort;

    private String biographyShort;

    private String biography;

    private Date birthDate;

    private String birthPlace;

    private Date deathDate;

    private String deathPlace;

    private String deathCause;

    private List<Feature> feature;

    private String realName;

    private Picture picture;

    private String trailerEmbed;

    private boolean hasTopFilmography;

    private List<Link> link;

    private List<Participation> participation;

    @JsonIgnore
    private List<Media> media;

    @JsonIgnore
    private List<News> news;

    @JsonIgnore
    private List<Festival> festivalAward;

    @JsonIgnore
    private List<Festival> festivalSection;

    private PersonStatistics statistics;

}
