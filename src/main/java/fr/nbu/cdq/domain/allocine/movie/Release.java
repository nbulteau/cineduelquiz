package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("release")
public class Release {

	private String releaseDate;

	private String reissueDate;

	private Code country;

	private Code releaseState;

	private Code releaseVersion;

	private Distributor distributor;

}
