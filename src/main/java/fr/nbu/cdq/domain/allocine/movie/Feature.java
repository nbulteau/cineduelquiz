package fr.nbu.cdq.domain.allocine.movie;

import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("feature")
public class Feature {
	private List<Code> category;

	private Number code;

	private Picture picture;

	private Publication publication;

	private String title;
}
