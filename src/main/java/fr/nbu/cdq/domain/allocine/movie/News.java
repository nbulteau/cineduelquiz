package fr.nbu.cdq.domain.allocine.movie;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("news")
@JsonIgnoreProperties(ignoreUnknown = true)
public class News {
  // Empty block
}
