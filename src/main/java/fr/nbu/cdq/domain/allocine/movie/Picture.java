package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

@Data
public class Picture {

    private String name;

    private String href;

    private String path;

}
