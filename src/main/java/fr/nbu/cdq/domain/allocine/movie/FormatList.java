package fr.nbu.cdq.domain.allocine.movie;

import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("formatList")
public class FormatList {

	private List<Code> productionFormat;

	private List<Code> projectionFormat;

	private List<Code> soundFormat;
}
