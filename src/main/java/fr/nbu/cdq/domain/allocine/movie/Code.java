package fr.nbu.cdq.domain.allocine.movie;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("code")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Code {

    private String label;

    private int code;

    public String get$() {
        return this.label;
    }

    public void set$(String $) {
        this.label = $;
    }

    @JsonProperty("$")
    public String getLabel() {
        return label;
    }

    @JsonProperty("$")
    public void setLabel(String label) {
        this.label = label;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
