package fr.nbu.cdq.domain.allocine.movie;

import java.util.Date;
import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("feed")
public class Feed {
	private Date updated;

	private int page;

	private int count;

	private int totalResults;

	private List<Movie> movie;
}
