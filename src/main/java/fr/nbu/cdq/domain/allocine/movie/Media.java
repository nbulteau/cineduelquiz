package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

final @Data @JsonTypeName("media") public class Media {
  // Empty block
}
