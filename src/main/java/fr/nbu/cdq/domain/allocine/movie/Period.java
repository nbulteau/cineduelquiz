package fr.nbu.cdq.domain.allocine.movie;

import java.util.Date;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("period")
public class Period {

    private Date dateStart;

    private Date dateEnd;

}
