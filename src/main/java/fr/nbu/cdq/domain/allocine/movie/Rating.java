package fr.nbu.cdq.domain.allocine.movie;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("rating")
public class Rating {

    private float note;

    private int count;

    public int get$() {
        return this.count;
    }

    public void set$(int $) {
        this.count = $;
    }

    public float getNote() {
        return note;
    }

    public void setNote(float note) {
        this.note = note;
    }

    @JsonProperty("$")
    public int getCount() {
        return count;
    }

    @JsonProperty("$")
    public void setCount(int count) {
        this.count = count;
    }
}
