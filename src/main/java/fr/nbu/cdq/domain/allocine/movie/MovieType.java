package fr.nbu.cdq.domain.allocine.movie;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("movieType")
public class MovieType extends Code {
  // Empty block
}
