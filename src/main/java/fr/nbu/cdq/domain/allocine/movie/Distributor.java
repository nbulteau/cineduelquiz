package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("distributor")
public class Distributor {
	private String name;

	private Number code;
}
