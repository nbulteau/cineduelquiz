package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

@Data
public class MovieResult {
    private Movie movie;
}
