package fr.nbu.cdq.domain.allocine.movie;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Getter
@Setter
@JsonTypeName("trailer")
public class Trailer extends Picture {

    private String code;
}
