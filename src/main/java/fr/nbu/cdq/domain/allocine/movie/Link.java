package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("link")
public class Link {
	private String rel;

	private String href;

	private String name;
}
