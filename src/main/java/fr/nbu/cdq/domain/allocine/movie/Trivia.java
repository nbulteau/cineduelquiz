package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("trivia")
public class Trivia {

    private int code;

    private Publication publication;

    private String title;

    private String body;

}
