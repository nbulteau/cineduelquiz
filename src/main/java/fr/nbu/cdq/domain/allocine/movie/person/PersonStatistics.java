package fr.nbu.cdq.domain.allocine.movie.person;

import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

import fr.nbu.cdq.domain.allocine.movie.Rating;

@Data
@JsonTypeName("statistic")
public class PersonStatistics {

    private int fanCount;

    private int movieCount;

    private int movieActorCount;

    private int movieDirectorCount;

    private int movieProducerCount;

    private int seriesCount;

    private int seriesActorCount;

    private int seriesDirectorCount;

    private int seriesProducerCount;

    private int newsCount;

    private int nominationCount;

    private int awardCount;

    private int rankTopStar;

    private int variationTopStar;

    private int careerDuration;

    private List<Rating> rating;

}
