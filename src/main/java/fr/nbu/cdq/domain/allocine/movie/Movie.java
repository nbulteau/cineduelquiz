package fr.nbu.cdq.domain.allocine.movie;

import java.util.Date;
import java.util.List;

import lombok.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("movie")
@Document
public class Movie {

    @Id
    private long code;

    @JsonIgnore
    private List<CastMember> alloCineCastMember;

    private List<CastMember> castMember;

    @JsonIgnore
    private CastingShort alloCineCastingShort;

    private CastingShort castingShort;

    @JsonIgnore
    private List<Feature> alloCineFeature;

    private List<Feature> feature;

    @JsonIgnore
    private List<Code> alloCineGenre;

    private List<Code> genre;

    private String keywords;

    @JsonIgnore
    private List<Link> alloCineLink;

    private List<Link> link;

    @JsonIgnore
    private MovieType alloCineMovieType;

    private MovieType movieType;

    @JsonIgnore
    private List<Code> alloCineNationality;

    private List<Code> nationality;

    @JsonIgnore
    private List<News> alloCineNews;

    private List<News> news;

    private String originalTitle;

    @JsonIgnore
    private Picture alloCinePoster;

    private Picture poster;

    private Number productionYear;

    @JsonIgnore
    private Release alloCineRelease;

    private Release release;

    private int runtime;

    @JsonIgnore
    private MovieStatistics alloCineStatistics;

    private MovieStatistics statistics;

    private String synopsis;

    private String synopsisShort;

    private List<Code> tag;

    @Indexed(name = "titleIndex", direction = IndexDirection.ASCENDING)
    private String title;

    @JsonIgnore
    private Trailer alloCineTrailer;

    private Trailer trailer;

    @JsonIgnore
    private List<Trivia> alloCineTrivia;

    private List<Trivia> trivia;

    @JsonIgnore
    private MovieCertificate alloCineMovieCertificate;

    private MovieCertificate movieCertificate;

    private Code color;

    @JsonIgnore
    private FormatList alloCineFormatList;

    private FormatList formatList;

    private List<Code> language;

    private String budget;

    private String trailerEmbed;

    @JsonIgnore
    private List<Media> defaultMedia;

    @JsonIgnore
    private List<Media> media;

    private List<BoxOffice> boxOffice;

    private boolean hasBluRay;

    private Date bluRayReleaseDate;

    private boolean hasDVD;

    private Date dvdReleaseDate;

    private boolean hasVOD;

    private boolean hasBroadcast;

    @JsonIgnore
    private Date nextBroadcast;

    private boolean hasShowtime;

    private boolean hasSVOD;

    @JsonIgnore
    private List<Review> helpfulPositiveReview;

    @JsonIgnore
    private List<Review> helpfulNegativeReview;
}
