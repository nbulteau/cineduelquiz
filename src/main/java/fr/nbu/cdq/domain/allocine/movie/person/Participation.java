package fr.nbu.cdq.domain.allocine.movie.person;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import fr.nbu.cdq.domain.allocine.movie.Code;
import fr.nbu.cdq.domain.allocine.movie.Movie;

@Data
@JsonTypeName("participation")
public class Participation {

    private Movie movie;

    @JsonIgnore
    private TvSerie tvseries;

    private Code activity;

    private String role;

}
