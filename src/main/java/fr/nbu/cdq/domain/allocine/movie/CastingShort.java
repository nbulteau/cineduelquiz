package fr.nbu.cdq.domain.allocine.movie;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("castingShort")
public class CastingShort {
	private String actors;

	private String directors;
}
