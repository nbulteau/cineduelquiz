package fr.nbu.cdq.domain.quiz;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.nbu.cdq.domain.util.CustomThemeSerializer;

@JsonSerialize(using = CustomThemeSerializer.class)
public enum Type {

    WitchActors("Quel acteur joue ce role ?",
            "Certains films bénéficient de casting déjà prestigieux. Saurez-vous retrouver lesquels d'entre eux réunissent ces stars ?"),
    ACastingWitchMovie(
            "Quel film avec ce casting ?",
            "Certains acteurs se ressemblent au point qu'on les confond souvent, d'autres ont une filmographie très riche avec des rôles inattendus. Saurez-vous associer ces films avec leurs têtes d'affiche ?"),
    FromWitchAnimationComeThisHero("Hero des films d'animation", "De quel film d'animation vient ce hero ?");

    private final String label;

    private final String desciption;

    private Type(String label, String desciption) {
        this.label = label;
        this.desciption = desciption;
    }

    public String getLabel() {
        return this.label;
    }

    public String getDesciption() {
        return this.desciption;
    }
}
