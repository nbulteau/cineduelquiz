package fr.nbu.cdq.domain.quiz;

import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("game")
public class Game {

    private Quiz quiz;

    private List<Player> players;
}
