package fr.nbu.cdq.domain.quiz;

public enum Difficulty {
    Noob, Expert, Nightmare;
}
