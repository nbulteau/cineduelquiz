package fr.nbu.cdq.domain.quiz;

import java.util.Map;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

import fr.nbu.cdq.domain.User;

@Data
@JsonTypeName("player")
public class Player {

    private User player;

    private Map<MultipleChoiceQuestion, Answer> answers;
}
