package fr.nbu.cdq.domain.quiz;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NonNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("quiz")
@Document
public class Quiz {

    @Id
    private String id;

    private String name;

    @NonNull
    private Type type;

    private Difficulty difficulty;

    private Theme theme;

    private List<MultipleChoiceQuestion> multipleChoiceQuestions = new ArrayList<>();

}
