package fr.nbu.cdq.domain.quiz;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("multiplechoicequestion")
public class MultipleChoiceQuestion {

    public final static int NB_ANSWSERS_BY_QUESTION = 4;

    @lombok.NonNull
    private String label;

    private List<Answer> wrongAnswsers = new ArrayList<>();

    private Answer correctAnswer;

}
