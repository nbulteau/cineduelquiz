package fr.nbu.cdq.domain.quiz;

import lombok.Data;
import lombok.NonNull;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Data
@JsonTypeName("answser")
public class Answer {

    @NonNull
    private Integer id;

    @NonNull
    private String label;

}
