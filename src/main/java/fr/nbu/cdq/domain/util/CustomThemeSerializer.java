package fr.nbu.cdq.domain.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import fr.nbu.cdq.domain.quiz.Type;

/**
 * Custom Jackson serializer for transforming a Type object to JSON.
 */
public class CustomThemeSerializer extends JsonSerializer<Type> {

    @Override
    public void serialize(Type value, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {
        generator.writeStartObject();
        generator.writeStringField("label", value.getLabel());
        generator.writeStringField("description", value.getDesciption());
        generator.writeEndObject();
    }
}
