package fr.nbu.cdq.config;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.datatype.joda.JodaModule;

import fr.nbu.cdq.domain.quiz.Type;
import fr.nbu.cdq.domain.util.CustomDateTimeDeserializer;
import fr.nbu.cdq.domain.util.CustomDateTimeSerializer;
import fr.nbu.cdq.domain.util.CustomLocalDateSerializer;
import fr.nbu.cdq.domain.util.CustomThemeSerializer;
import fr.nbu.cdq.domain.util.ISO8601LocalDateDeserializer;

@Configuration
public class JacksonConfiguration {

    @Bean
    public JodaModule jacksonJodaModule() {
        JodaModule module = new JodaModule();
        module.addSerializer(DateTime.class, new CustomDateTimeSerializer());
        module.addDeserializer(DateTime.class, new CustomDateTimeDeserializer());
        module.addSerializer(LocalDate.class, new CustomLocalDateSerializer());
        module.addDeserializer(LocalDate.class, new ISO8601LocalDateDeserializer());
        module.addSerializer(Type.class, new CustomThemeSerializer());
        return module;
    }
}
