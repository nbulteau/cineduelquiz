package fr.nbu.cdq.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.nbu.cdq.repository.file.FilePictureRepository;
import fr.nbu.cdq.web.servlet.PictureServlet;

@Configuration
public class ServletConfiguration {

    @Bean
    public ServletRegistrationBean servletRegistrationBean(FilePictureRepository filePosterRepository, FilePictureRepository filePictureRepository) {
        return new ServletRegistrationBean(new PictureServlet(filePosterRepository, filePictureRepository), "/pictureServlet/*");
    }

    @Bean
    public FilePictureRepository filePosterRepository(@Value("${filerepository.fileposters.path}") String repositoryPath,
            @Value("${filerepository.fileposters.suffix}") String suffix) {
        return new FilePictureRepository(repositoryPath, suffix);
    }

    @Bean
    public FilePictureRepository filePictureRepository(@Value("${filerepository.filepictures.path}") String repositoryPath,
            @Value("${filerepository.filepictures.suffix}") String suffix) {
        return new FilePictureRepository(repositoryPath, suffix);
    }
}
