package fr.nbu.cdq.service;

import javax.inject.Inject;

import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.nbu.cdq.Application;
import fr.nbu.cdq.config.MongoConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
@Import(MongoConfiguration.class)
public class QuizServiceTest {

    @Inject
    QuizService quizService;

    // @Test
    // public void generateWitchActorsThemeQuizTest() {
    // Type theme = new WitchActorsQuizGenerator();
    // Quiz quiz = quizService.generateQuiz(theme);
    // System.out.println(quiz);
    // }
    //
    // @Test
    // public void generateACastingWitchMovieThemeQuizTest() {
    // Type theme = new ACastingWitchMovieQuizGenerator();
    // Quiz quiz = quizService.generateQuiz(theme);
    // System.out.println(quiz);
    // }
}
