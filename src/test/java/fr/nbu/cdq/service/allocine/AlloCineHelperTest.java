package fr.nbu.cdq.service.allocine;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.nbu.cdq.domain.allocine.movie.Movie;

public class AlloCineHelperTest {

    private static final long LE_FACTEUR_SONNE_TOUJOURS_DEUX_FOIS = 1l;

    private static final long OBLIVION = 27405l;

    private static final long YOLANDE_MOREAU = 3153l;

    @Test
    public void testReadFilePath() throws IOException {
        URL url = Thread.currentThread().getContextClassLoader().getResource("movies/");
        Path moviesPath = Paths.get(URI.create(url.toString()));

        // Count json files
        long nbJsonFiles = Files.list(moviesPath).filter(path -> path.toString().endsWith(".json")).count();
        List<Movie> movies = AlloCineHelper.read(moviesPath);
        Assert.assertEquals(nbJsonFiles, movies.size());
    }

    @Test
    public void testReadFile1() throws URISyntaxException {
        String path = "movies/" + LE_FACTEUR_SONNE_TOUJOURS_DEUX_FOIS + ".json";
        URL url = Thread.currentThread().getContextClassLoader().getResource(path);
        File file = new File(url.getPath());

        Movie movie = AlloCineHelper.read(file);
        Assert.assertEquals(LE_FACTEUR_SONNE_TOUJOURS_DEUX_FOIS, movie.getCode());
    }

    @Test
    public void testReadFile2() throws JsonProcessingException {
        String path = "movies/" + OBLIVION + ".json";
        URL url = Thread.currentThread().getContextClassLoader().getResource(path);
        File file = new File(url.getPath());

        Movie movie = AlloCineHelper.read(file);
        Assert.assertEquals(OBLIVION, movie.getCode());
    }

    @Test
    public void testRetrieveMovie() throws RetrieveException {
        Movie movie = AlloCineHelper.retrieveMovie(OBLIVION);
        Assert.assertEquals(OBLIVION, movie.getCode());
    }

    @Test
    public void testRetrievePerson() throws RetrieveException {
        Movie movie = AlloCineHelper.retrievePerson(YOLANDE_MOREAU);
        Assert.assertEquals("YOLANDE_MOREAU", movie.getCode());
    }

    @Test
    @Ignore
    public void testRetrieveMovielist() throws RetrieveException {
        List<Movie> movieList = AlloCineHelper.retrieveMovielist();
        Assert.assertTrue(movieList.size() > 0);
    }

}
