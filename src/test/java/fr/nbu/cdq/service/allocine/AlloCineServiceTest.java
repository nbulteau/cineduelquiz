package fr.nbu.cdq.service.allocine;

import static org.assertj.core.api.StrictAssertions.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.nbu.cdq.Application;
import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.domain.allocine.movie.person.Person;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class AlloCineServiceTest {

  private static final long OBLIVION = 27405;

  private static final long LE_FACTEUR_SONNE_TOUJOURS_DEUX_FOIS = 1;

  private static final long YOLANDE_MOREAU = 3153l;

  @Inject
  AlloCineService alloCineService;

  @Test
  public void retrieveAndSaveMovie1Test() {
    final Movie movie = alloCineService.retrieveAndSaveMovie(OBLIVION);
    assertNotNull(movie);
    assertThat(movie.getCode() == OBLIVION);
  }

  @Test
  public void retrieveAndSaveMovie2Test() {
    final Movie movie = alloCineService.retrieveAndSaveMovie(LE_FACTEUR_SONNE_TOUJOURS_DEUX_FOIS);
    assertNotNull(movie);
    assertThat(movie.getCode() == LE_FACTEUR_SONNE_TOUJOURS_DEUX_FOIS);
  }

  @Test
  public void retrieveAndSavePerson() {
    final Person person = alloCineService.retrieveAndSavePerson(YOLANDE_MOREAU);
    assertNotNull(person);
    Assert.assertEquals("YOLANDE_MOREAU", person.getCode());
  }

  @Test
  public void retrieveMovieListTest() {
    final List<Movie> movies = alloCineService.retrieveMovieList();
    assertThat(movies.size() > 0);
  }

  @Test
  public void saveAllFromFileRepositoryTest() throws IOException {
    alloCineService.saveAllFromFileRepository();
  }

}
