package fr.nbu.cdq.repository.file;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import fr.nbu.cdq.domain.allocine.movie.Movie;

public class FileMovieRepositoryTest {

    private static final long OBLIVION = 27405;

    private final FileMovieRepository fileMovieRepository = new FileMovieRepository("C:/allocine/jsons", ".json");

    @Test
    public void findOneTest() {
        Movie movie = fileMovieRepository.findOne(OBLIVION);

        assertThat(movie.getCode() == OBLIVION);
    }

    @Test
    public void countTest() throws IOException {
        long nbMovies = fileMovieRepository.count();

        assertTrue(nbMovies == 110015);
    }
}
