package fr.nbu.cdq.repository;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.nbu.cdq.Application;
import fr.nbu.cdq.config.MongoConfiguration;
import fr.nbu.cdq.domain.allocine.movie.Movie;
import fr.nbu.cdq.service.allocine.AlloCineHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
@Import(MongoConfiguration.class)
public class MovieRepositoryTest {

    private static final long OBLIVION = 27405;

    @Inject
    MovieRepository movieRepository;

    @Test
    public void saveTest() {
        String path = "movies/" + OBLIVION + ".json";
        URL url = Thread.currentThread().getContextClassLoader().getResource(path);
        File file = new File(url.getPath());

        Movie movie = AlloCineHelper.read(file);

        movieRepository.save(movie);
        movie = movieRepository.findOne(OBLIVION);
        assertTrue(movie.getCode() == OBLIVION);
    }
}
